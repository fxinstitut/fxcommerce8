<?php
/*
|--------------------------------------------------------------------------
| AdminUrlData.php
|--------------------------------------------------------------------------
| Created by Shawn Legge
| This class is responsible for returning a list of common urls that will be
| used by vue in the admin site
*/

namespace App\Library\Data;

use App\Http\Controllers\Admin\ProductsController;
use App\Http\Controllers\Admin\API\StatesAPIController;

use App\Http\Controllers\Admin\API\CategoriesAPIController;

use App\Http\Controllers\Admin\CategoriesController;

use App\Http\Controllers\Admin\API\OrdersAPIController;

use App\Http\Controllers\Admin\SalesController;

use App\Http\Controllers\Admin\API\TaxesAPIController;

use App\Http\Controllers\Admin\TaxesController;

use App\Http\Controllers\Admin\API\ProductsAPIController;



class AdminUrlData
{
    public static function get()
    {
        return [
            'product_url' => action([ProductsController::class, 'index']),
            'product_api_url' => action([ProductsAPIController::class, 'index']),
            'tax_url' => action([TaxesController::class, 'index']),
            'tax_api_url' => action([TaxesAPIController::class, 'index']),
            'sale_url' => action([SalesController::class, 'index']),
            'order_url' => action([OrdersAPIController::class, 'show'], ['order' => null]),
            'category_url' => action([CategoriesController::class, 'index']),
            'category_api_url' => action([CategoriesAPIController::class, 'index']),
            'state_api_url' => action([StatesAPIController::class, 'index'])
        ];
    }
}
