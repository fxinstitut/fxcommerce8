<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SeedImageTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Models\Image', 20)->create();
    }
}
